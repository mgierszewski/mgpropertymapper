//
//  DateProperty.h
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 24.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "MGProperty.h"

#define _MGDateProperty(name, format) ([MGDateProperty propertyWithName:(@#name) dateFormat:(format)])

@interface MGDateProperty : MGProperty
@property (nonatomic, readonly) NSString *dateFormat;

- (instancetype)initWithName:(NSString *)name dateFormat:(NSString *)dateFormat;
+ (instancetype)propertyWithName:(NSString *)name dateFormat:(NSString *)dateFormat;
@end
