//
//  MGArrayProperty.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 24.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGProperty.h"

#define _MGCollectionProperty(name, itemClass, itemMapping, uniqueProperties) [MGCollectionProperty propertyWithName:(@#name) collectionItemClass:([itemClass class]) mapping:(itemMapping) coreDataUniqueProperties:(uniqueProperties)]

@interface MGCollectionProperty : MGProperty
@property (nonatomic, readonly) Class collectionItemClass;
@property (nonatomic, readonly) NSDictionary *mapping;

- (instancetype)initWithName:(NSString *)name collectionItemClass:(Class)itemClass mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)coreDataUniquePropertyNames;;

+ (instancetype)propertyWithName:(NSString *)name collectionItemClass:(Class)itemClass mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)coreDataUniquePropertyNames;;
@end
