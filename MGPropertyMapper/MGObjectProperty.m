//
//  MGObjectProperty.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 24.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGObjectProperty.h"
#import "MGPropertyMapperHelper.h"

@interface MGObjectProperty ()
@property (nonatomic, strong) Class objectClass;
@property (nonatomic, strong) NSDictionary *mapping;
@property (nonatomic, strong) NSArray *coreDataUniquePropertyNames;
@end

@implementation MGObjectProperty

+ (instancetype)propertyWithName:(NSString *)name objectClass:(__unsafe_unretained Class)objectClass mapping:(NSDictionary *)mapping  coreDataUniqueProperties:(id)coreDataUniquePropertyNames;
{
    return [[MGObjectProperty alloc] initWithName:name objectClass:objectClass mapping:mapping coreDataUniqueProperies:coreDataUniquePropertyNames];
}

- (instancetype)initWithName:(NSString *)name objectClass:(__unsafe_unretained Class)objectClass mapping:(NSDictionary *)mapping coreDataUniqueProperies:(id)coreDataUniquePropertyNames;
{
    self = [super initWithName:name];
    if (self)
    {
        self.objectClass = objectClass;
        self.mapping = mapping;

        if ([coreDataUniquePropertyNames isKindOfClass:[NSArray class]])
        {
            self.coreDataUniquePropertyNames = coreDataUniquePropertyNames;
        }
        else if (coreDataUniquePropertyNames && [coreDataUniquePropertyNames isKindOfClass:[NSString class]])
        {
            self.coreDataUniquePropertyNames = @[coreDataUniquePropertyNames];
        }
    }
    return self;
}

- (void)mapValue:(id)value toObject:(id)object
{
    id childObject = [MGPropertyMapperHelper createChildObjectOfClass:self.objectClass parentObject:object initWithData:value mapping:self.mapping coreDataUniqueProperties:self.coreDataUniquePropertyNames];
    if (childObject)
    {
        [super mapValue:childObject toObject:object];
    }
    else
    {
        [super mapValue:value toObject:object];
    }
}
@end
