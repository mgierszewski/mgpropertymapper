//
//  Property.m
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 23.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "MGProperty.h"
#import <objc/runtime.h>

@interface MGProperty ()
@property (nonatomic, strong) NSString *propertyName;
@property (nonatomic, strong) Class propertyClass;

- (BOOL)matchWithPropertyOfObject:(id)object;

- (void)setValue:(id)value forObject:(id)object;
@end

@implementation MGProperty

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self)
    {
        self.propertyName = name;
    }
    return self;
}

+ (instancetype)propertyWithName:(NSString *)name
{
    return [[MGProperty alloc] initWithName:name];
}

// check if object has property with the same name
- (BOOL)matchWithPropertyOfObject:(id)object
{
    objc_property_t property = class_getProperty([object class], [self.propertyName UTF8String]);
	
	// check if property exists
    if (property == 0)
    {
        return NO;
    }

    unsigned int propertyAttributeCount = 0;
    objc_property_attribute_t *propertyAttributeList = property_copyAttributeList(property, &propertyAttributeCount);
    for (int i = 0; i < propertyAttributeCount; i++)
    {
        objc_property_attribute_t attribute = propertyAttributeList[i];
		
		// compare type and attribute name
        if (strcmp(attribute.name, "T") == 0)
        {
            NSString *classString = [NSString stringWithUTF8String:attribute.value];
            classString = [classString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"@\""]];
            
            self.propertyClass = NSClassFromString(classString);
        }
    }
    return YES;
}

- (void)mapValue:(id)value toObject:(id)object
{
    // check if property belongs to object
    if (![self matchWithPropertyOfObject:object])
    {
        return;
    }
    
    if ([value isKindOfClass:[NSNull class]])
    {
        // assign nil
        [self setValue:nil forObject:object];
    }
    else
    {
        if (self.propertyClass == nil)
        {
            // for cases when the property is of 'id' type
            [self setValue:value forObject:object];
        }
        else if ([value isKindOfClass:self.propertyClass])
        {
            // if value's and property's classes match - assign value
            [self setValue:value forObject:object];
        }
        else if (self.propertyClass == [NSURL class])
        {
            // create URL if possible
            NSURL *url = nil;
            if ([value isKindOfClass:[NSString class]])
            {
                url = [NSURL URLWithString:value];
            }
            
            if (url)
            {
                [self setValue:url forObject:object];
            }
        }
        else if (self.propertyClass == [NSOrderedSet class])
        {
            // create ordered set if possible
            NSOrderedSet *orderedSet = nil;
            if ([value isKindOfClass:[NSArray class]])
            {
                orderedSet = [NSOrderedSet orderedSetWithArray:value];
            }
            
            if (orderedSet)
            {
                [self setValue:orderedSet forObject:object];
            }
        }
        else if (self.propertyClass == [NSSet class])
        {
            // create set if possible
            NSSet *set = nil;
            if ([value isKindOfClass:[NSArray class]])
            {
                set = [NSSet setWithArray:value];
            }
            
            if (set)
            {
                [self setValue:set forObject:object];
            }
        }
        else if (self.propertyClass == [NSDate class])
        {
            // create date if possible
            NSDate *date = nil;
            if ([value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSString class]])
            {
                date = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
            }
            
            if (date)
            {
                [self setValue:date forObject:object];
            }
        }
    }
}

- (void)setValue:(id)value forObject:(id)object
{
    @try
    {
        if ([object isKindOfClass:[NSManagedObject class]])
        {
            [object willChangeValueForKey:self.propertyName];
        }

        if (value)
        {
            [object setValue:value forKey:self.propertyName];
        }
        else
        {
            [object setNilValueForKey:self.propertyName];
        }
        
        if ([object isKindOfClass:[NSManagedObject class]])
        {
            [object didChangeValueForKey:self.propertyName];
        }
    }
    @catch (NSException *exception)
    {
        // NSLog(@"%@", exception);
    }
    
}

@end
