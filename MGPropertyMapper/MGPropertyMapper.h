//
//  PropertyMapper.h
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 23.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "MGProperty.h"
#import "MGDateProperty.h"
#import "MGCollectionProperty.h"
#import "MGObjectProperty.h"

@interface MGPropertyMapper : NSObject
+ (void)mapDictionary:(NSDictionary *)dictionary toObject:(id)object usingMapping:(NSDictionary *)mapping;
@end
