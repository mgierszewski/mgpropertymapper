//
//  MGPropertyMapperHelper.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGPropertyMapperHelper.h"
#import "MGPropertyMapper.h"
#import <CoreData/CoreData.h>

@implementation MGPropertyMapperHelper

+ (id)createChildObjectOfClass:(Class)class parentObject:(id)parentObject initWithData:(id)data mapping:(NSDictionary *)mapping coreDataUniqueProperties:(NSArray *)uniqueAttributeNames
{
    if (data == nil || ([data isKindOfClass:[NSDictionary class]] && [(NSDictionary *)data count] == 0))
    {
        return nil;
    }
    
    id object = nil;
    @try
    {
        if ([class isSubclassOfClass:[NSManagedObject class]] && [parentObject isKindOfClass:[NSManagedObject class]])
        {
            NSManagedObjectContext *context = [(NSManagedObject *)parentObject managedObjectContext];
            NSManagedObjectModel *model = context.persistentStoreCoordinator.managedObjectModel;
            
            NSInteger entityIndex = [model.entities indexOfObjectPassingTest:^BOOL(NSEntityDescription* entity, NSUInteger idx, BOOL *stop) {
                return [entity.managedObjectClassName isEqual:NSStringFromClass(class)];
            }];
            
            if (model && entityIndex != NSNotFound)
            {
                NSEntityDescription *entityDescription = model.entities[entityIndex];
                
                // create valueObject to match non-optional attributes
                if ([uniqueAttributeNames count] > 0)
                {
                    id testObject = [[class alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:nil];
                    [MGPropertyMapper mapDictionary:data toObject:testObject usingMapping:mapping];
                    
                    // check if object with those non-optional attributes exitsts
                    NSMutableArray *predicates = [NSMutableArray array];
                    [uniqueAttributeNames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        NSPredicate *subPredicate = [NSPredicate predicateWithFormat:@"%K == %@", obj, [testObject valueForKey:obj]];
                        [predicates addObject:subPredicate];
                    }];
                    
                    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityDescription.name];
                    fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
                    fetchRequest.fetchLimit = 1;
                    
                    NSError *fetchError = nil;
                    NSArray *fetchResult = [context executeFetchRequest:fetchRequest error:&fetchError];
                    if ([fetchResult count] > 0)
                    {
                        // object exists
                        object = [fetchResult firstObject];
                    }
                }
                
                if (!object)
                {
                    object = [[class alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:context];
                }
            }
        }

        // create object if it doesn't exist
        if (!object && ![class isSubclassOfClass:[NSManagedObject class]])
        {
            object = [[class alloc] init];
        }
    }
    @catch (NSException *exception) {}
    
    [MGPropertyMapper mapDictionary:data toObject:object usingMapping:mapping];
    return object;
}

@end
