//
//  MGObjectProperty.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 24.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import <MGPropertyMapper/MGPropertyMapper.h>

#define _MGObjectProperty(name, object_class, object_mapping, unique_properties) [MGObjectProperty propertyWithName:(@#name) objectClass:([object_class class]) mapping:(object_mapping) coreDataUniqueProperties:(unique_properties)]

@interface MGObjectProperty : MGProperty
@property (nonatomic, readonly) Class objectClass;
@property (nonatomic, readonly) NSDictionary *mapping;

- (instancetype)initWithName:(NSString *)name objectClass:(Class)objectClass mapping:(NSDictionary *)mapping coreDataUniqueProperies:(id)coreDataUniquePropertyNames;

+ (instancetype)propertyWithName:(NSString *)name objectClass:(Class)objectClass mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)coreDataUniquePropertyNames;
@end
