//
//  Property.h
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 23.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

#define _MGProperty(name) [MGProperty propertyWithName:(@#name)]

@interface MGProperty : NSObject
@property (nonatomic, readonly) NSString *propertyName;
@property (nonatomic, readonly) Class propertyClass;

- (instancetype)initWithName:(NSString *)name;
+ (instancetype)propertyWithName:(NSString *)name;

- (void)mapValue:(id)value toObject:(id)object;
@end
