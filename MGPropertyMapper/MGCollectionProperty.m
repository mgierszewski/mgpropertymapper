//
//  MGArrayProperty.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 24.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "MGCollectionProperty.h"
#import "MGPropertyMapper.h"
#import "MGPropertyMapperHelper.h"

@interface MGCollectionProperty ()
@property (nonatomic, strong) Class collectionItemClass;
@property (nonatomic, strong) NSDictionary *mapping;
@property (nonatomic, strong) NSArray *coreDataUniquePropertyNames;
@end

@implementation MGCollectionProperty

+ (instancetype)propertyWithName:(NSString *)name collectionItemClass:(Class)itemClass mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)coreDataUniquePropertyNames;
{
    return [[MGCollectionProperty alloc] initWithName:name collectionItemClass:itemClass mapping:mapping coreDataUniqueProperties:coreDataUniquePropertyNames];
}

- (instancetype)initWithName:(NSString *)name collectionItemClass:(Class)itemClass mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)coreDataUniquePropertyNames;
{
    self = [super initWithName:name];
    if (self)
    {
        self.collectionItemClass = itemClass;
        self.mapping = mapping;
        
        if ([coreDataUniquePropertyNames isKindOfClass:[NSArray class]])
        {
            self.coreDataUniquePropertyNames = coreDataUniquePropertyNames;
        }
        else if (coreDataUniquePropertyNames && [coreDataUniquePropertyNames isKindOfClass:[NSString class]])
        {
            self.coreDataUniquePropertyNames = @[coreDataUniquePropertyNames];
        }
    }
    return self;
}

- (void)mapValue:(id)value toObject:(id)object
{
    // map array of values
    NSArray *array = nil;
    if ([value isKindOfClass:[NSArray class]])
    {
        NSArray *valueArray = (NSArray *)value;
        NSMutableArray *outputArray = [NSMutableArray array];
        [valueArray enumerateObjectsUsingBlock:^(id dictionary, NSUInteger idx, BOOL *stop) {
            id childObject = [MGPropertyMapperHelper createChildObjectOfClass:self.collectionItemClass parentObject:object initWithData:dictionary mapping:self.mapping coreDataUniqueProperties:self.coreDataUniquePropertyNames];
            if (childObject)
            {
                [outputArray addObject:childObject];
            }
        }];
        array = [NSArray arrayWithArray:outputArray];
    }
    
    if (array)
    {
        [super mapValue:array toObject:object];
    }
    else
    {
        [super mapValue:value toObject:object];
    }
}

@end
