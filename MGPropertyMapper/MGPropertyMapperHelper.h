//
//  MGPropertyMapperHelper.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MGPropertyMapperHelper : NSObject

+ (id)createChildObjectOfClass:(Class)class parentObject:(id)parentObject initWithData:(id)data mapping:(NSDictionary *)mapping coreDataUniqueProperties:(id)uniqueAttributeNames;

@end
