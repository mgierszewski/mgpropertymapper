//
//  DateProperty.m
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 24.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "MGDateProperty.h"

@interface MGDateProperty ()
@end

@implementation MGDateProperty

- (instancetype)initWithName:(NSString *)name dateFormat:(NSString *)dateFormat
{
    self = [super initWithName:name];
    if (self)
    {
        _dateFormat = dateFormat;
    }
    return self;
}

+ (instancetype)propertyWithName:(NSString *)name dateFormat:(NSString *)dateFormat
{
    return [[MGDateProperty alloc] initWithName:name dateFormat:dateFormat];
}

- (void)mapValue:(id)value toObject:(id)object
{
    NSDate *date = nil;
    if ([value isKindOfClass:[NSString class]] && self.dateFormat != nil)
    {
        // use date format
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
		dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        dateFormatter.dateFormat = self.dateFormat;

        date = [dateFormatter dateFromString:value];
    }
        
    // assign value (if not nil)
    if (date)
    {
        [self mapValue:date toObject:object];
    }
    else
    {
        [super mapValue:value toObject:object];
    }
}

@end