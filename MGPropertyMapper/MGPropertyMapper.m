//
//  PropertyMapper.m
//  MGPropertyMapper
//
//  Created by Maciej Gierszewski on 23.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import "MGPropertyMapper.h"
#import <objc/runtime.h>

@interface MGPropertyMapper ()
@end

// -- 
@implementation MGPropertyMapper

+ (void)mapDictionary:(NSDictionary *)valueDictionary toObject:(id)object usingMapping:(NSDictionary *)mapping
{
    if (![valueDictionary isKindOfClass:[NSDictionary class]])
    {
        return;
    }
    
    // key - name of field in valueDictionary
    // value - object's property to which value of key should be assigned;
    //         value can be also a dictionary
    [mapping enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        
        if ([value isKindOfClass:[NSDictionary class]])
        {
            // nested mapping
            id submapping = mapping[key];
            [MGPropertyMapper mapDictionary:valueDictionary[key] toObject:object usingMapping:submapping];
        }
        else if ([value isKindOfClass:[MGProperty class]])
        {
            // map property
            [(MGProperty *)value mapValue:valueDictionary[key] toObject:object];
        }
        else if ([value isKindOfClass:[NSString class]])
        {
            // create property from string and map as property
            if (valueDictionary[key])
            {
                [MGPropertyMapper mapDictionary:@{key: valueDictionary[key]} toObject:object usingMapping:@{key: [MGProperty propertyWithName:value]}];
            }
        }
    }];
}

@end