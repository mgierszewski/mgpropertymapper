Pod::Spec.new do |s|
s.name         = "MGPropertyMapper"
s.version      = "0.0.1"
s.summary      = "Mapping Array/Dictionary structure to Key-Value objects"
s.author       = { "Maciej Gierszewski" => "m.gierszewski@futuremind.com" }
s.platform     = :ios, '6.0'
s.source       = { :git => "https://bitbucket.org/mgierszewski/mgpropertymapper.git", :tag => "0.0.1" }
s.source_files = 'MGPropertyMapper'
s.requires_arc = true
s.license		= { :type => 'WTFPL' }
s.homepage		= 'https://bitbucket.org/mgierszewski/mgpropertymapper.git'
end
