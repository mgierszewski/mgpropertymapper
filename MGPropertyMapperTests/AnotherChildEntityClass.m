//
//  AnotherChildEntityClass.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "AnotherChildEntityClass.h"
#import "ParentEntityClass.h"


@implementation AnotherChildEntityClass

@dynamic parent;

@end
