//
//  Model.h
//
//  Created by Maciek Gierszewski on 03.12.2014.
//  Copyright (c) 2014 Future Mind sp. z o. o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CoreData : NSObject
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (NSError *)saveContext:(NSManagedObjectContext *)context;
@end
