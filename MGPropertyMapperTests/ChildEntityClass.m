//
//  ChildEntityClass.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "ChildEntityClass.h"
#import "ParentEntityClass.h"


@implementation ChildEntityClass

@dynamic parent;

@end
