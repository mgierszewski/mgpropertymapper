//
//  ParentEntityClass.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class AnotherChildEntityClass, ChildEntityClass;

@interface ParentEntityClass : BaseEntity

@property (nonatomic, retain) ChildEntityClass *child;
@property (nonatomic, retain) NSSet *children;
@end

@interface ParentEntityClass (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(AnotherChildEntityClass *)value;
- (void)removeChildrenObject:(AnotherChildEntityClass *)value;
- (void)addChildren:(NSSet *)values;
- (void)removeChildren:(NSSet *)values;

@end
