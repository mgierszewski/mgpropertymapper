//
//  ChildEntityClass.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@class ParentEntityClass;

@interface ChildEntityClass : BaseEntity

@property (nonatomic, retain) ParentEntityClass *parent;

@end
