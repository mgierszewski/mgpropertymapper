//
//  Model.m
//
//  Created by Maciek Gierszewski on 03.12.2014.
//  Copyright (c) 2014 Future Mind sp. z o. o. All rights reserved.
//

#import "CoreData.h"

@interface CoreData ()
@property (nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, readonly) NSManagedObjectModel *managedObjectModel;

@property (nonatomic, readonly) NSURL *storeURL;
@property (nonatomic, readonly) NSURL *modelURL;
@end

@implementation CoreData

#pragma mark - Initializer

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        self.managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
    }
    return self;
}

#pragma mark - Core Data Setup

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    NSPersistentStoreCoordinator *persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    [persistentStoreCoordinator addPersistentStoreWithType:NSInMemoryStoreType
                                             configuration:nil
                                                       URL:nil
                                                   options:nil
                                                     error:nil];
    return persistentStoreCoordinator;
}

- (NSManagedObjectModel*)managedObjectModel
{
    NSManagedObjectModel *model = [NSManagedObjectModel mergedModelFromBundles:[NSBundle allBundles]];
    return model;
}

- (NSURL*)storeURL
{
    NSURL* documentsDirectory = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                                       inDomain:NSUserDomainMask
                                                              appropriateForURL:nil
                                                                         create:YES
                                                                          error:NULL];
    return [documentsDirectory URLByAppendingPathComponent:@"MGPropertyMapper.sqlite"];
}

- (NSURL*)modelURL
{
    return [[NSBundle mainBundle] URLForResource:@"MGPropertyMapper" withExtension:@"momd"];
}

#pragma mark - Save

- (NSError *)saveContext:(NSManagedObjectContext *)context
{
    NSError *saveError = nil;
    [context save:&saveError];
    
	return saveError;
}

@end
