//
//  MGPropertyMapperTests.m
//  MGPropertyMapperTests
//
//  Created by Maciej Gierszewski on 24.04.2014.
//  Copyright (c) 2014 Maciej Gierszewski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MGPropertyMapper.h"
#import "ChildEntityClass.h"
#import "ParentEntityClass.h"
#import "AnotherChildEntityClass.h"
#import "CoreData.h"

@interface TestObject : NSObject
@property (nonatomic, strong) NSString *stringProperty;
@property (nonatomic, strong) NSNumber *numberProperty;
@property (nonatomic, strong) NSDate *dateProperty;

@property (nonatomic, strong) NSArray *arrayProperty;
@end

@implementation TestObject
@end

#pragma mark - Test

@interface MGPropertyMapperTests : XCTestCase
@property (nonatomic, strong) CoreData *coreData;
@end

@implementation MGPropertyMapperTests

- (void)setUp
{
    [super setUp];
    
    self.coreData = [CoreData new];
}

- (void)tearDown
{
    [super tearDown];
    
    [self.coreData.context rollback];
}

- (void)testBasicMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"string": @"abcd",
                                 @"number": @{ @"value": @13
                                               },
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *mapping = @{ @"string": [MGProperty propertyWithName:@"stringProperty"],
                               @"number": @{@"value": [MGProperty propertyWithName:@"numberProperty"]},
                               };

    // --------
    // map data
    // --------
    TestObject *object = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object usingMapping:mapping];
    
    // ----
    // test
    // ----
    XCTAssertEqualObjects(object.stringProperty, dictionary[@"string"], @"");
    XCTAssertEqualObjects(object.numberProperty, dictionary[@"number"][@"value"], @"");
}

- (void)testNullMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"string": [NSNull null]
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *mapping = @{ @"string": _MGProperty(stringProperty),
                               };
    
    // --------
    // map data
    // --------
    TestObject *object = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object usingMapping:mapping];
    
    // ----
    // test
    // ----
    XCTAssertNil(object.stringProperty, @"");
}

- (void)testDateFormatMapping
{
    // ----
    // data
    // ----
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    NSDictionary *dictionary = @{@"date": @"2014-05-10"};
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *mapping = @{ @"date": _MGDateProperty(dateProperty, dateFormatter.dateFormat) };
    
    // --------
    // map data
    // --------
    TestObject *object = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object usingMapping:mapping];

    // ----
    // test
    // ----
    NSDate *date = [dateFormatter dateFromString:dictionary[@"date"]];
    
    XCTAssertEqualWithAccuracy([object.dateProperty timeIntervalSinceDate:date], 0, 0.1, @"");
}

- (void)testDateTimestampMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"date": @1398349023,
                                 @"stringDate": @"1398349043"
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *mapping1 = @{ @"date": _MGProperty(dateProperty) };
    NSDictionary *mapping2 = @{ @"stringDate": _MGProperty(dateProperty) };
    
    // --------
    // map data
    // --------
    TestObject *object1 = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object1 usingMapping:mapping1];
    
    TestObject *object2 = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object2 usingMapping:mapping2];
    
    // ----
    // test
    // ----
    XCTAssertEqualWithAccuracy([object1.dateProperty timeIntervalSince1970], [dictionary[@"date"] doubleValue], 0.1, @"");
    XCTAssertEqualWithAccuracy([object2.dateProperty timeIntervalSince1970], [dictionary[@"stringDate"] doubleValue], 0.1, @"");
}

- (void)testArrayMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"items": @[ @{@"name": @"John",
                                                @"age": @29
                                                },
                                              @{@"name": @"Dave",
                                                @"age": @34
                                                },
                                              @{@"name": @"Kevin",
                                                @"age": @21
                                                }
                                              ]
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *mapping = @{@"items": [MGCollectionProperty propertyWithName:@"arrayProperty"
                                                           collectionItemClass:[TestObject class]
                                                                       mapping:@{@"name": @"stringProperty",
                                                                                 @"age": @"numberProperty"}
                                                      coreDataUniqueProperties:nil]};
    
    // --------
    // map data
    // --------
    TestObject *object = [TestObject new];
    [MGPropertyMapper mapDictionary:dictionary toObject:object usingMapping:mapping];
    
    
    // ----
    // test
    // ----
    XCTAssertEqual([object.arrayProperty count], [dictionary[@"items"] count]);
    
    [object.arrayProperty enumerateObjectsUsingBlock:^(TestObject *subObject, NSUInteger idx, BOOL *stop) {
        XCTAssertEqualObjects(subObject.stringProperty, dictionary[@"items"][idx][@"name"]);
        XCTAssertEqualObjects(subObject.numberProperty, dictionary[@"items"][idx][@"age"]);
    }];
}

- (void)testCoreDataSetMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"name": @"Anna",
                                 @"age": @46,
                                 @"children": @[ @{@"name": @"John",
                                                   @"age": @13
                                                   },
                                                 @{@"name": @"Dave",
                                                   @"age": @18
                                                   },
                                                 @{@"name": @"Kevin",
                                                   @"age": @9
                                                   }
                                                 ]
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *childMapping = @{@"name": @"name", @"age": @"number"};
    NSDictionary *mapping = @{@"name": @"name",
                              @"age": @"number",
                              @"children": _MGCollectionProperty(children, AnotherChildEntityClass, childMapping, nil)};
    
    // --------
    // map data
    // --------
    ParentEntityClass *parent = [NSEntityDescription insertNewObjectForEntityForName:@"ParentEntity" inManagedObjectContext:self.coreData.context];
    [MGPropertyMapper mapDictionary:dictionary toObject:parent usingMapping:mapping];
    
    // ----
    // test
    // ----
    XCTAssertEqualObjects(parent.name, dictionary[@"name"], @"");
    XCTAssertEqualObjects(parent.number, dictionary[@"age"], @"");
    
    XCTAssertEqual([parent.children count], [dictionary[@"children"] count]);
    
    [parent.children enumerateObjectsUsingBlock:^(AnotherChildEntityClass *child, BOOL *stop) {
        NSArray *filteredArray = [dictionary[@"children"] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name == %@ AND age == %@", child.name, child.number]];
        XCTAssertEqual([filteredArray count], 1);
    }];
}

- (void)testCoreDataObjectMapping
{
    // ----
    // data
    // ----
    NSDictionary *dictionary = @{@"name": @"Doug",
                                 @"age": @54,
                                 @"child": @{@"name": @"John",
                                             @"age": @29
                                             },
                                 };
    
    // --------------
    // mapping schema
    // --------------
    NSDictionary *childMapping = @{@"name": @"name", @"age": @"number"};
    NSDictionary *mapping = @{@"name": @"name",
                              @"age": @"number",
                              @"child": _MGObjectProperty(child, ChildEntityClass, childMapping, nil)
                              };
    
    // --------
    // map data
    // --------
    ParentEntityClass *parent = [NSEntityDescription insertNewObjectForEntityForName:@"ParentEntity" inManagedObjectContext:self.coreData.context];
    [MGPropertyMapper mapDictionary:dictionary toObject:parent usingMapping:mapping];
    
    // ----
    // test
    // ----
    XCTAssertEqualObjects(parent.name, dictionary[@"name"], @"");
    XCTAssertEqualObjects(parent.number, dictionary[@"age"], @"");
    XCTAssertEqualObjects(parent.child.name, dictionary[@"child"][@"name"], @"");
    XCTAssertEqualObjects(parent.child.number, dictionary[@"child"][@"age"], @"");
}

- (void)testCoreDataUniqueProperties
{
	// ----
	// data
	// ----
	NSDictionary *dictionary = @{@"name": @"Doug",
								 @"age": @54,
								 @"child": @{@"name": @"John",
											 @"age": @29
											 },
								 };
	
	// -------------
	// create object
	// -------------
	ChildEntityClass *child = [NSEntityDescription insertNewObjectForEntityForName:@"ChildEntity" inManagedObjectContext:self.coreData.context];
	child.name = @"John";
	child.number = @14;
	
	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"ChildEntity"];
	XCTAssertEqual([self.coreData.context countForFetchRequest:fetchRequest error:nil], 1);
	
	// --------------
	// mapping schema
	// --------------
	NSDictionary *childMapping = @{@"name": @"name", @"age": @"number"};
	NSDictionary *mapping = @{@"name": @"name",
							  @"age": @"number",
							  @"child": _MGObjectProperty(child, ChildEntityClass, childMapping, @[@"name"])
							  };
	
	// --------
	// map data
	// --------
	ParentEntityClass *parent = [NSEntityDescription insertNewObjectForEntityForName:@"ParentEntity" inManagedObjectContext:self.coreData.context];
	[MGPropertyMapper mapDictionary:dictionary toObject:parent usingMapping:mapping];
	
	// ----
	// test
	// ----
	XCTAssertEqual([self.coreData.context countForFetchRequest:fetchRequest error:nil], 1);
}

@end
