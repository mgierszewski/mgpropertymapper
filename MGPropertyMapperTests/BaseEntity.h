//
//  BaseEntity.h
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BaseEntity : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * number;

@end
