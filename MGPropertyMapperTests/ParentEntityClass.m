//
//  ParentEntityClass.m
//  MGPropertyMapper
//
//  Created by Maciek Gierszewski on 25.02.2015.
//  Copyright (c) 2015 Maciej Gierszewski. All rights reserved.
//

#import "ParentEntityClass.h"
#import "AnotherChildEntityClass.h"
#import "ChildEntityClass.h"


@implementation ParentEntityClass

@dynamic child;
@dynamic children;

@end
